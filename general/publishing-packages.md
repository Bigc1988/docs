# Package build

## Overview

Packages are built and released in the following steps:

1. Packages builds automatically run when a new [release is tagged](rake-tasks.md#releaseversion)
1. Once the [build pipelines](https://dev.gitlab.org/gitlab/omnibus-gitlab/pipelines?scope=tags) complete these are privately available for internal deploys
1. Builds are manually published using [the instructions below](#publishing-packages)

Running the release task `/chatops run publish X.X.X`, will
create a [pipeline for a tag on dev.gitlab.org](https://dev.gitlab.org/gitlab/omnibus-gitlab/pipelines?scope=tags)
on the omnibus-gitlab repository.

Pipelines for tags in this repository create the final packages.
Every package will be automatically built and pushed to a *private* package
repository. This repository will then be used for deploys to staging, canary
and production environments of GitLab.com.

Once we are confident that the GitLab release is working in GitLab.com environments,
we can publish the packages to public repositories. Public repositories are
being used by our users and customers, so make sure that you only publish
after successful deploy.

Package build is divided roughly with following stages:

* Package build (Automatic)
* Package upload to private repository (Automatic)
* Package upload to public repositories (Manual)
* Docker image build (Automatic)
* Docker image release (Manual)
* QA image and AWS AMI release (Manual)
* Raspberry Pi package build (Automatic)
* Raspberry Pi package release (Manual)

## Publishing packages

Releasing packages to the public are handled using manual jobs in the pipeline.
There are manual jobs for releasing the following
1. Package for each supported OS (Raspberry Pi is a separate job)
1. GitLab Docker image
1. GitLab QA image
1. AMI for AWS

Note: In addition to the release jobs, the pipeline also contains an `upgrade`
job that measures the time taken to upgrade from last minor version release to
the current release.

![Package build pipeline](images/release.png)

*Note*: Raspberry Pi package builds can take a very long time to complete.
The release can be completed without the finished Raspberry Pi package build.
But do remember to release the Raspberry Pi package once the build is completed.

### FAQ

#### Why is the upload to the private repository a separate job?

To avoid having to rebuild the package if the only failure is during package push.

#### What happens if one of the package builds failed?

In this case, retry the failed job. If the issue persists, escalate to the
Build team. This is a release blocker.

#### What happens if one of the uploads to the *private* repository failed?

In this case, finalizing release is not possible. Retry the job and if the
upload fails again, escalate to the Build team.

#### What happens if one of the uploads to the *public* repositories failed?

In this case, retry the failed job. If the upload fails again, escalate to the
Build team.

#### What happens if the either of the all-in-one release jobs fail

In this case, you can go ahead and manually trigger each of the individual
package or image release jobs. The all-in-one release jobs basically does the
same thing.
