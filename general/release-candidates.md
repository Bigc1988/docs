# Creating Release Candidates

Release Candidates (RCs) are pre-release versions of the next version of
GitLab CE and EE. The first RC, appropriately called RC1, is typically created
as close as possible during the release of each month.

There should be no code changes between the final RC and what is released to the
public.

## About the "Release Candidate" naming

We call them "Release Candidates" as this simplifies our releasing/packaging
tools and scripts. This approach is coherent with packages.gitlab.com since our
RC packages are available under [`gitlab/unstable`].

## Release Candidates creation.

The Release Candidates creation is part of the Monthly Release process and follows roughly the subsequent steps:

* Near the 22nd, Release Managers select the last green auto deploy branch. This one
will be used as the "cut" for the release, meaning everything up to the commit in
`auto-deploy` branch will be included in the release.
* Stable branches for GitLab and GitLab-FOSS are created based from that branch. 
Stable branches are the sole source of future releases for that version.
* Release Managers tag a new version, creating a new Release Candidate.

Steps are detailed in the "17th" section of the [Monthly Release] template.

[Monthly Release]: https://gitlab.com/gitlab-org/release-tools/blob/master/templates/monthly.md.erb

---

[Return to Guides](../README.md#guides)
