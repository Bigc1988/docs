# Overview

The [auto-deploy process overview](auto-deploy.md) provides details on the
building pieces of the process, a schedule overview and answers a few frequently
asked questions.

Previous contents of this document were used to transition from the monthly deploy cadence to the new deploy system. For the most current information, 
checkout the [Releases Handbook page](https://about.gitlab.com/handbook/engineering/releases/). 

